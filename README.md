# MergeTB TestPlan Site

## Contributing Test Plans

The format of a test plan consists of 3 parts

- **Coverage report**: What parts of the
  [spec](https://gitlab.com/mergetb/validation/specs) does this testplan cover?
- **Description**: Describe the plan
- **Status report**: A link to the corresponding CI pipeline status and source
  code.

An example is spelled out below. This site takes advantage of
[MDX](https://mdxjs.com/) which allows for React components in markdown. React
components are provided for defining spec coverage, linking to pipelines and
source code.

```markdown
## Simpile Topology Modeling

<Coverage spec={[
    '1.1.1.1', 
    '1.1.2.1',
    '1.1.2.2',
    '1.1.2.3',
    '1.1.2.4',
]} />

This test plan covers the basics for Merge topology modeling. In this test
suite, a set of topologies are defined in the Python
[mergexp](https://pypi.org/project/mergexp/) modeling library. These models are
executed to produce XIR and that XIR is read in by a Go test program using the
Go XIR bindings.

<PipelineBadge job='simple-topo' />
<SourceBadge test='experiment/simple-topo' />
```

## General Development Instructions

This website is built using [Docusaurus 2](https://v2.docusaurus.io/), a modern static website generator.

### Installation

```
$ yarn
```

### Local Development

```
$ yarn start
```

This command starts a local development server and open up a browser window. Most changes are reflected live without having to restart the server.

### Build

```
$ yarn build
```

This command generates static content into the `build` directory and can be served using any static contents hosting service.

### Deployment

```
$ GIT_USER=<Your GitHub username> USE_SSH=true yarn deploy
```

If you are using GitHub pages for hosting, this command is a convenient way to build the website and push to the `gh-pages` branch.
