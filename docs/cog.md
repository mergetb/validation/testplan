---
id: cog
title: Cog
---

:::note
This is a placeholder for future stuff.

But it *is* rigged up to use the [cogs repository](https://gitlab.com/mergetb/tech/cogs) as the origin
for badges for test results and sources.

This mainly to provide a working example of how one might link to test fulfillment hosted in a repo
other than the default one.
:::

import {Coverage, PipelineBadge, SourceBadge} from './coverage'

<PipelineBadge test_repo='tech/cogs' job='simple-topo' />
<SourceBadge test_repo='tech/cogs' test='' />
