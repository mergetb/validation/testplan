---
id: about
title: About
---

## Guidlines for Testing

The basic rule of thumb for whether a test plan belongs here is simple.

> Does the test directly assert the functionality of a numbered item in one of
> the MergeTB specs?

The MergeTB specs are located here 

- https://gitlab.com/mergetb/validation/specs

Every test plan should assert specific functionality from a spec. 
For example one could write a test that asserts Experimentation specification 
[1.1.2.1](https://mergetb.gitlab.io/-/validation/specs/-/jobs/528940460/artifacts/experimentation.html#_topology)
that single node experiment topologies are supported. There are many ways this
capability can be asserted. For example

- Asserting that a single node experiment expressed by XIR in Python is read
  back by the Go XIR bindings as the same single node experiment.

- Asserting that the portal's model service accepts single node experiments.

- Asserting that single node experiments are realizable.

- Asserting that single node experiments materialize correctly.

All of the above may be parts of different test plans that have different
perspectives on the same specification item. Or they could be part of a
vertically integrated test plan hyper focused on exhaustively asserting a single
specification from multiple angles. The exact structural relationship between
test plans and the test specifications defined only by directional links from
plans to specs. This allows for flexibility in how to structure plans around
specs, but also allows for accountability through coverage calculations.

## Contributing Test Plans

The format of a test plan consists of 3 parts

- **Coverage report**: What parts of the
  [spec](https://gitlab.com/mergetb/validation/specs) does this test plan cover?
- **Description**: Describe the plan
- **Status report**: A link to the corresponding CI pipeline status and source
  code.

An example is spelled out below. This site takes advantage of
[MDX](https://mdxjs.com/) which allows for React components in markdown. React
components are provided for defining spec coverage, linking to pipelines and
source code.

```markdown
## Simpile Topology Modeling

<Coverage spec={[
    '1.1.1.1', 
    '1.1.2.1',
    '1.1.2.2',
    '1.1.2.3',
    '1.1.2.4',
]} />

This test plan covers the basics for Merge topology modeling. In this test
suite, a set of topologies are defined in the Python
[mergexp](https://pypi.org/project/mergexp/) modeling library. These models are
executed to produce XIR and that XIR is read in by a Go test program using the
Go XIR bindings.

<PipelineBadge job='simple-topo' />
<SourceBadge test='experiment/simple-topo' />
```
