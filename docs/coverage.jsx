import React from 'react';

class Coverage extends React.Component {
    render() {
	console.log(this.props.spec);
	return(
	    <div
		style={{
		    backgroundColor: '#f4f4f4',
		    borderRadius: '2px',
		    color: 'var(--ifm-menu-color-active)',
		    padding: '0.2rem',
		    'margin-top': '10px',
		    'margin-bottom': '10px',
		    backgroundColor: 'var(--ifm-menu-color-background-active)'
		}}>
		<span><b>Coverage:</b></span>&nbsp;
		<span class="spec-items">
		    {this.props.spec.reduce((a, x) => a + ", " + x)}
		</span>
	    </div>
	);
    }
}

class PipelineBadge extends React.Component {

    render() {
	return(
	    <a class="badge" href={"https://gitlab.com/mergetb/" + this.props.test_repo + "/pipelines"}>
		<img src={"https://gitlab.com/mergetb/" + this.props.test_repo + "/badges/master/pipeline.svg?job="+ this.props.job}/>
	    </a>
	);
    }

}

PipelineBadge.defaultProps = {
    test_repo: "validation/tests",
}

class SourceBadge extends React.Component {

    render() {
	return(
	    <a class="badge" href={"https://gitlab.com/mergetb/" + this.props.test_repo + "/blob/master/" + this.props.test}>
	    <img src="https://img.shields.io/badge/test-source-blue"/>
	    </a>
	);
    }

}

SourceBadge.defaultProps = {
    test_repo: "validation/tests",
}

export {
    Coverage,
    PipelineBadge,
    SourceBadge
}
