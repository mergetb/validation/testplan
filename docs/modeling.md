---
id: modeling
title: Modeling
---

import {Coverage, PipelineBadge, SourceBadge} from './coverage'

## Simpile Topology Modeling

<Coverage spec={[
    '1.1.1.1', 
    '1.1.2.1',
    '1.1.2.2',
    '1.1.2.3',
    '1.1.2.4',
]} />

This test plan covers the basics for Merge topology modeling. In this test
suite, a set of topologies are defined in the Python
[mergexp](https://pypi.org/project/mergexp/) modeling library. These models are
executed to produce XIR and that XIR is read in by a Go test program using the
Go XIR bindings.

<PipelineBadge job='simple-topo' />
<SourceBadge test='experiment/simple-topo' />

