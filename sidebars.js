module.exports = {
  someSidebar: {
    About: ['about'],
    Experimentation: ['modeling', 'instantiation', 'access'],
    Commissioning: ['tbmodeling'],
    'Portal Administration': ['mergectl'],
    'Facility Administration': ['cog'],
    Facility: [
        'device-provisioning', 
        'xp-infra',
        'device-interconnection',
        'facility-interconnection',
    ],
  },
};
