module.exports = {
  title: 'MergeTB TestPlan',
  tagline: 'Acceptance test plans for the MergeTB Spec',
  url: 'https://your-docusaurus-test-site.com',
  baseUrl: '/',
  favicon: 'img/merge.svg',
  organizationName: 'facebook', // Usually your GitHub org/user name.
  projectName: 'docusaurus', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'MergeTB TestPlan',
      logo: {
        alt: 'My Site Logo',
        src: 'img/merge.svg',
      },
      links: [
        {
          to: 'docs/about',
          activeBasePath: 'docs',
          label: 'About',
          position: 'left',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [],
      copyright: `Copyright © ${new Date().getFullYear()} Information Sciences Institute`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
