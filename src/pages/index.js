import React from 'react';
import classnames from 'classnames';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';
import {Coverage, PipelineBadge, SourceBadge} from '../../docs/coverage'

const features = [ ];

function Feature({imageUrl, title, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={classnames('col col--4', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  );
}

function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title=''
      description={`${siteConfig.title}`}>
      <header className={classnames('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
            <Link
              className={classnames(
                'button button--outline button--secondary button--lg button--blue',
                styles.getStarted,
              )}
              to={useBaseUrl('docs/modeling')}>
              The Plans
            </Link>
          </div>
        </div>
      </header>
      <main>
        <div class="cols">
            <div class="col">
                <h2> Experimentation </h2>
                <div class="test-item">
                    <span class="test-header"> Simple Topo </span>
                    <PipelineBadge job='simple-topo' />
                </div>
            </div>
            <div class="col">
                <h2> Commissioning </h2>
                <div class="test-item">
                    <span class="test-header"> Resource Lifecycle </span>
                    <PipelineBadge job='resource-lifecycle' />
                </div>
            </div>
            <div class="col">
                <h2> Portal Admin </h2>
                <div class="test-item">
                    <span class="test-header"> User Management </span>
                    <PipelineBadge job='user-management' />
                </div>
            </div>
            <div class="col">
                <h2> Facility Admin </h2>
                <div class="test-item">
                    <span class="test-header"> LinkPlan Tests </span>
                    <PipelineBadge job='linkplan-tests' />
                </div>
            </div>
        </div>
      </main>
    </Layout>
  );
}

export default Home;
